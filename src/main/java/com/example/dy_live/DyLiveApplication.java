package com.example.dy_live;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication(scanBasePackages = "com.example")
public class DyLiveApplication {

    public static void main(String[] args) {
        SpringApplication.run(DyLiveApplication.class, args);
    }

}
