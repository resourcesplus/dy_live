<%--
  Created by IntelliJ IDEA.
  User: shanjianyu
  Date: 2022/4/22
  Time: 10:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="/dyLive/static/css/pop.css" rel="stylesheet">
    <script src="/dyLive/static/js/jquery.min.js"></script>
    <style type="text/css">

     </style>
</head>
<body>
    <input type="hidden" id="id" value="${id}">
    <div id="msgContent" style="height: 95%;overflow:auto;">
    </div>


<script type="application/javascript">
    $(function(){
        var color = ["#e78167", "#7b9774", "#74978a","#749297","#747c97",
                    "#8f7497","#977477","#748297","#cd5b5b","#df9848",
                    "#d4d481","#9eb974","#84b974","#74b9a0","#74b3b9",
                    "#74a3b9","#5e8ba0","#605ea0","#875ea0","#a05e5f"];
        var id = $("#id").val();

        var socket;

        if(typeof(WebSocket) == "undefined") {
            console.log("您的浏览器不支持WebSocket");
        } else {
            console.log("您的浏览器支持WebSocket");
            //实现化WebSocket对象，指定要连接的服务器地址与端口  建立连接
            socket = new WebSocket("ws://localhost:8088/dyLive/websocket/"+id);
            //打开事件
            socket.onopen = function() {
                console.log("Socket 已打开");
                // socket.send("这是来自客户端的消息" + location.href + new Date());
            };
            //获得消息事件
            socket.onmessage = function(msg) {
                var data = $.parseJSON(msg.data);
                console.log(data);
                console.log(data.img);
                console.log(data.msg);
                //发现消息进入    开始处理前端触发逻辑
                var index = Math.floor(Math.random()*19);
                var bgColor = color[index];
                var html = '<div class="pop" style="background-color: '+bgColor+'">';
                html += '<div style="background-color: black;height: 100px;width: 100px;float: left;background: url('+data.img+');border-radius: 50px;"></div>';
                html += data.msg;
                html += '</div>'
                $("#msgContent").append(html);

                var h4 = $('#msgContent').prop("scrollHeight");
                // $('#msgContent').scrollTo({h4, behavior: 'smooth'})

                $("#msgContent").animate({ scrollTop: h4 },50);

                var popLength = $(".pop").length;
                if(popLength > 20){
                    $(".pop")[0].remove();
                }
            };
            //关闭事件
            socket.onclose = function() {
                console.log("Socket已关闭");
            };
            //发生了错误事件
            socket.onerror = function() {
                alert("Socket发生了错误");
                //此时可以尝试刷新页面
            }
        }
    })
</script>
</body>
</html>